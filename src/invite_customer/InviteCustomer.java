/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invite_customer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.json.JSONObject;

/**
 *
 * @author fm
 */
public class InviteCustomer {

    //constants
    private static double DUBLIN_OFFICE_LATITUDE = 53.339428;
    private static double DUBLIN_OFFICE_LONGITUDE = -6.257664;
    private static String INPUT_FILENAME = "customers.txt";
    private static String OUTPUT_FILENAME = "output.txt";
    private static double KM_LIMIT = 100;

    private static ArrayList<Customer> customersList;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            readFromFile(INPUT_FILENAME);
            if (!customersList.isEmpty()) {
                writeToFile(customersList);
            }
        } catch (IOException e) {
            System.err.println(INPUT_FILENAME + " not found");
        }
    }

    //read data from customers.txt file
    public static void readFromFile(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        System.out.println("----------Customers to invite----------");
        customersList = new ArrayList<Customer>();
        while ((line = reader.readLine()) != null) {
//            System.out.println("" + line);
            JSONObject jsono = new JSONObject(line);
            double customer_lat = jsono.getDouble("latitude");
            double customer_lon = jsono.getDouble("longitude");
            Customer c = new Customer();
            c.setLatitude(customer_lat);
            c.setLongitude(customer_lon);
            c.setName(jsono.getString("name"));
            c.setUserId(jsono.getInt("user_id"));
            double distanceFromOffice = distance(DUBLIN_OFFICE_LATITUDE, DUBLIN_OFFICE_LONGITUDE, customer_lat, customer_lon);
            c.setDistanceFromOffice(distanceFromOffice);
            if (distanceFromOffice < KM_LIMIT) {
//                System.out.println("UserId:" + c.getUserId() + "\nName:" + c.getName() + "\nDistance from office:" + distanceFromOffice + "km\n------------");
                customersList.add(c);
            }
        }
        reader.close();
    }

    //save customer list to output file
    public static void writeToFile(ArrayList<Customer> list) {
        //sort customer list in ascending order
        Collections.sort(list);
        try {
            File file = new File(OUTPUT_FILENAME);
            if (file.createNewFile()) {
//                System.out.println("File created: " + file.getName());
            } else {
                String textToAppend = "----------customers to invite----------\n";
                for (int i = 0; i < list.size(); i++) {
                    String line1 = "UserId: " + list.get(i).getUserId();
                    String line2 = "Name: " + list.get(i).getName();
                    String line4 = "Distance from Office: " + list.get(i).getDistanceFromOffice() + "km";
                    String line3 = "-------------------------------------------------------------------";
                    textToAppend = textToAppend + line1 + "\n" + line2 + "\n" + line3 + "\n";
                    System.out.println(line1 + "\n" + line2 + "\n" + line4 + "\n" + line3);
                }
                Files.write(Paths.get(OUTPUT_FILENAME), textToAppend.getBytes(), StandardOpenOption.CREATE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //calculate distance between each user and dublin office
    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        //convert degree to radians
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        //haversine formula  
        double diffLon = lon2 - lon1;
        double diffLat = lat2 - lat1;
        double hEquation = Math.pow(Math.sin(diffLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(diffLon / 2), 2);
        double result = 2 * Math.asin(Math.sqrt(hEquation));

        //radius of earth 
        double radiusEarth = 6371;
        return (result * radiusEarth);
    }

}
