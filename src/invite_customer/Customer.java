/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invite_customer;

/**
 *
 * @author fm
 */
public class Customer implements Comparable< Customer> {

    private double latitude;
    private double longitude;
    private Integer userId;
    private String name;
    private double distanceFromOffice;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistanceFromOffice() {
        return distanceFromOffice;
    }

    public void setDistanceFromOffice(double distanceFromOffice) {
        this.distanceFromOffice = distanceFromOffice;
    }

    @Override
    public int compareTo(Customer c) {
        return this.getUserId().compareTo(c.getUserId());
    }
    
}
