# Invite Customers

Take Home Test

**Technical problem**

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).
## Setup

### Getting Started
- This project is done in `Java` programming language using Netbeans IDE
- Requires `JSON` (https://mvnrepository.com/artifact/org.json/json/20200518)

### Run the project
- Open the project in Netbeans IDE, and follow the step below
```bash
Run InviteCustomer.java 
```
- Running this class will print the result to 'output.txt'

### Test the project
```bash
Run InviteCustomerTest.java 
```