/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invite_customer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fm
 */
public class InviteCustomerTest {
    
    public InviteCustomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class InviteCustomer.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        InviteCustomer.main(args);
    }

    /**
     * Test of readFromFile method, of class InviteCustomer.
     */
    @Test
    public void testReadFromFile() throws Exception {
        System.out.println("readFromFile");
        String filename = "customers.txt";
        InviteCustomer.readFromFile(filename);
    }
    
    
    /**
     * Test of readFromFile method, of class InviteCustomer.
     */
    @Test
    public void testWriteToFile() throws Exception {
        System.out.println("writeToFile");
        String filename = "output.txt";
        InviteCustomer.writeToFile(null);
    }

    /**
     * Test of distance method, of class InviteCustomer.
     */
    @Test
    public void testDistance() {
        System.out.println("distance");
        double lat1 = 53.339428;
        double lon1 = -6.257664;
        double lat2 = 53.2451022;
        double lon2 = -6.238335;
        double result = InviteCustomer.distance(lat1, lon1, lat2, lon2);
        assertEquals( 10.56693628886791,0,result);
    }
    
}
